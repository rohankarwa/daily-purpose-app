package app.android.theapp.registeration;

import java.util.ArrayList;

public class RegisterAppsImpl implements RegisterAppsInterface{

	@Override
	public ArrayList<AppInformation> registerAllApps() {
		ArrayList<AppInformation> allAppInfo = new ArrayList<AppInformation>();
	
		allAppInfo.add(new AppInformation("Calculate BMI and BMR", "app.android.theapp.BMIActivity"));
		allAppInfo.add(new AppInformation("Weather Information", "app.android.theapp.WeatherActivity"));
		allAppInfo.add(new AppInformation("Indian Railway PNR Status", "app.android.theapp.PNRActivity"));
		allAppInfo.add(new AppInformation("Get and Share Current Location", "app.android.theapp.CurrentLocationActivity"));
		allAppInfo.add(new AppInformation("Word Meaning", "app.android.theapp.WordMeaningActivity"));
		allAppInfo.add(new AppInformation("Mobile Number Information", "app.android.theapp.MobileNumberInformationActivity"));
		allAppInfo.add(new AppInformation("Horoscope", "app.android.theapp.HoroscopeActivity"));
		
		return allAppInfo;
	}

}
