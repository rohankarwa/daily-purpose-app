package app.android.theapp.registeration;

public class AppInformation {
	
	private String appName;
	private String appIntent;
	
	public AppInformation(String appName, String appIntent) {
		this.appName = appName;
		this.appIntent = appIntent;
	}
	
	public String getAppName() {
		return appName;
	}
	
	public String getAppIntent() {
		return appIntent;
	}
}
