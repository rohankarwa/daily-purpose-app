package app.android.theapp;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import app.android.theapp.integration.mobileinformation.MobileNumberDetails;
import app.android.theapp.integration.mobileinformation.MobileNumberInformation;

public class MobileNumberInformationActivity extends Activity{
	private Button mobileNumberInformationButton;
	private ListView mobileNumberInformationListView;
	private EditText mobileNumberInformationEditText;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobilenumberinformation);
        
        mobileNumberInformationButton = (Button) findViewById(R.id.mobileNumberInformationbtn);
        mobileNumberInformationEditText = (EditText) findViewById(R.id.mobileNumberInformationEditText);
        mobileNumberInformationListView = (ListView) findViewById(R.id.mobileNumberInformationListView);
        
        mobileNumberInformationButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String mobileEntered = mobileNumberInformationEditText.getText().toString();
				MobileNumberInformation mobileNumberInformation = new MobileNumberInformation();
				MobileNumberDetails mobileNumberDetails = mobileNumberInformation.getMobileInformation(mobileEntered);

				setResultList(mobileNumberDetails);
			}
		});
        
	}
	
	private void setResultList(MobileNumberDetails mobileNumberDetails) {
		
		ArrayList<String> mobileDetailsList = new ArrayList<String>();
		mobileDetailsList.add(mobileNumberDetails.getMobileNumber());
		mobileDetailsList.add(mobileNumberDetails.getLocation());
		mobileDetailsList.add(mobileNumberDetails.getOperator());
		mobileDetailsList.add(mobileNumberDetails.getSignalling());
        ListAdapter listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mobileDetailsList );
        mobileNumberInformationListView.setAdapter(listAdapter);
	}

}
