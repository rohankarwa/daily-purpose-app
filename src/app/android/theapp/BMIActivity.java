package app.android.theapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.android.theapp.integration.bmi.BMI;

public class BMIActivity extends Activity {
	private EditText weightText;
	private EditText heightText;
	private TextView resultView;
	private TextView resultLabel;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bmilayout);
        
        weightText = (EditText) findViewById(R.id.row1EditText);
      //  weight = weightText.getText().toString();
        
        heightText = (EditText) findViewById(R.id.row2EditText);
     //   height = heightText.getText().toString();
        
        resultView = (TextView) findViewById(R.id.result);
        Button resultButton = (Button) findViewById(R.id.bmibutton);
        
        resultLabel = (TextView) findViewById(R.id.resultLabel);
        
        resultButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				resultLabel.setVisibility(0);
		//		System.out.println("1HEIGHT: " + height.trim() + "\nWEIGHT = " + weight.trim());
				BMI bmi = new BMI(heightText.getText().toString(), weightText.getText().toString());
		//		BMI bmi = new BMI("173.4", "84.2");
		        resultView.setText(bmi.getBMI());
				
			}
		});
	}
}
