package app.android.theapp.integration.mobileinformation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobileNumberInformation {
	private String BASE_URL = "http://trace.bharatiyamobile.com/?numb=";
	public MobileNumberInformation() {}
	
	public MobileNumberDetails getMobileInformation(String mobileNumber) {
		String url = generateUrl(mobileNumber);
		String content = getContentFromUrl(url);
		if(content.equalsIgnoreCase("FAILED")) {
			MobileNumberDetails mobileNumberReturn = new MobileNumberDetails("Not Found", 
					mobileNumber, "Not Found", "Not Found");
			return mobileNumberReturn;
		}
		
		MobileNumberDetails mobileNumberDetails = parseContentAndGetMobileDetails(content, mobileNumber);
		
		return mobileNumberDetails;
	}
	
	private MobileNumberDetails parseContentAndGetMobileDetails(String content, String mobileNumber) {
		String mobileNumerNew = "";
		String operator = "";
		String signalling = "";
		String location = "";
		
		ArrayList<String> requiredBlocks = getListFromPattern(content, "<div class='numberstyle'>.*?</span>");
		if(requiredBlocks.size() == 0) {
			MobileNumberDetails mobileNumberReturn = new MobileNumberDetails("Not Found", 
					mobileNumber, "Not Found", "Not Found");
			return mobileNumberReturn;
		}
		
		for(int i = 0; i< requiredBlocks.size(); i++) {
			String currentBlock = requiredBlocks.get(i); 
			currentBlock = replaceToBlank(currentBlock, "<.*?>").trim();
			switch(i) {
				case 0:	 { mobileNumerNew = currentBlock; break; }
				case 1:	 { location = currentBlock; break; }
				case 2:	 { operator = currentBlock; break; }
				case 3:	 { signalling = currentBlock; break; }
			}
		}
		
		MobileNumberDetails mobileNumberDetails = new MobileNumberDetails(location, mobileNumerNew, operator, signalling);
		return mobileNumberDetails;
	}

	private String generateUrl(String mobileNumber) {
		String url = BASE_URL + mobileNumber;
		return url;
	}

	private String getContentFromUrl(String url) {
		String sourceLine;
	    String content = "";
	        
	    // The URL address of the page to open.
	    URL address = null;
	    URLConnection urlConnection = null;
	       
		try {
				address = new URL(url);
				urlConnection = address.openConnection();
		} catch (MalformedURLException e) {
				e.printStackTrace();
				return "FAILED";
		} catch (IOException e) {
				e.printStackTrace();
				return "FAILED";
		}
		urlConnection.setReadTimeout(60000);
		urlConnection.setConnectTimeout(60000);
		urlConnection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
	    InputStream pageInput = null;
		try {
			pageInput = urlConnection.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CAUGHT");
			return "FAILED";
		}
		
	    BufferedReader source;
		source = new BufferedReader(new InputStreamReader(pageInput));

	    // Append each new HTML line into one string. Add a tab character.
	    try {
			while ((sourceLine = source.readLine()) != null)
				        content += sourceLine + "\t";
		} catch (IOException e) {
			return "FAILED";
		}
	    
	    return content;
	}
	
	private String replaceToBlank(String content, String pattern) {
		Pattern style = Pattern.compile(pattern);
		Matcher mstyle = style.matcher(content);
		while (mstyle.find()) content = mstyle.replaceAll("");
		return content;
	}
	
	
	private ArrayList<String> getListFromPattern(String content, String pattern) {
	    ArrayList<String> output = new ArrayList<String>();
	    Pattern div_1 = Pattern.compile(pattern);
	    Matcher mscript_1 = div_1.matcher(content);
	    while (mscript_1.find()) {
	        output.add(mscript_1.group());
	    }
	    return output;
	}
}
