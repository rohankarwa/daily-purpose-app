package app.android.theapp.integration.mobileinformation;

public class MobileNumberDetails {
	private String location;
	private String mobileNumber;
	private String operator;
	private String signalling;
	
	public MobileNumberDetails(String location, String mobileNumber, String operator, String signalling) {
		this.location = location;
		this.mobileNumber = mobileNumber;
		this.operator = operator;
		this.signalling = signalling;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getSignalling() {
		return signalling;
	}
	public void setSignalling(String signalling) {
		this.signalling = signalling;
	}
	public String toString() {
		return mobileNumber + "~" +
				location + "~" +
				operator + "~" +
				signalling;
	}
}
