package app.android.theapp.integration.horoscope;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Horoscope {
	
	private HashMap<String, String> horoscopeHashMap = new HashMap<String, String>();
	private static final String HOROSCOPE_FEED_URL = "http://www.astrology.com/horoscopes/daily-horoscope.rss"; 
	public static final String ARIES = "Aries";
	public static final String TAURUS = "Taurus";
	public static final String GEMINI = "Gemini";
	public static final String CANCER = "Cancer";
	public static final String LEO = "Leo";
	public static final String VIRGO = "Virgo";
	public static final String LIBRA = "Libra";
	public static final String SCORPIO = "Scorpio";
	public static final String SAGITTARIS = "Sagittarius";
	public static final String CAPRICON = "Capricorn";
	public static final String AQUARIUS = "Aquarius";
	public static final String PISCES = "Pisces";
	public Horoscope() {}
	
	public String getHoroscope(String sunSign)  {
		prepareHoroscopeFromRSSFeed();
		String prediction = horoscopeHashMap.get(sunSign);
		return prediction;
	}
	
	private void prepareHoroscopeFromRSSFeed() {
		int count = 0;
		String content = getContentFromUrl(HOROSCOPE_FEED_URL);
		ArrayList<String> horoscopeBlocks = getListFromPattern(content, "<item>.*?</item>");
		for(String currentHoroscopeBlock : horoscopeBlocks) {
			String prediction  = "";
			ArrayList<String> descriptionBlocks = getListFromPattern(currentHoroscopeBlock, "<description>.*?</description>");
			for(String currentDescription : descriptionBlocks) {
				ArrayList<String> paraBlocks = getListFromPattern(currentDescription, "<p>.*?</p>");
				if(paraBlocks.size() != 0) {
					prediction = replaceToBlank(paraBlocks.get(0), "<.*?>").trim();
				} else {
					ArrayList<String> paraBlocksAgain = getListFromPattern(currentDescription, "&lt;p&gt;.*?&lt;/p&gt;&lt;");
					if(paraBlocksAgain.size() != 0 ) {
						prediction = replaceToBlank(paraBlocksAgain.get(0), "&lt;p&gt;").trim();
						prediction = replaceToBlank(prediction, "&lt;/p&gt;&lt;").trim();
					}
				}
			}
			
			switch(count) {
				case 0: {horoscopeHashMap.put(ARIES, prediction); break; }
				case 1: {horoscopeHashMap.put(TAURUS, prediction); break; }
				case 2: {horoscopeHashMap.put(GEMINI, prediction); break; }
				case 3: {horoscopeHashMap.put(CANCER, prediction); break; }
				case 4: {horoscopeHashMap.put(LEO, prediction); break; }
				case 5: {horoscopeHashMap.put(VIRGO, prediction); break; }
				case 6: {horoscopeHashMap.put(LIBRA, prediction); break; }
				case 7: {horoscopeHashMap.put(SCORPIO, prediction); break; }
				case 8: {horoscopeHashMap.put(SAGITTARIS, prediction); break; }
				case 9: {horoscopeHashMap.put(CAPRICON, prediction); break; }
				case 10: {horoscopeHashMap.put(AQUARIUS, prediction); break; }
				case 11: {horoscopeHashMap.put(PISCES, prediction); break; }
			}
			count++;
		}
	}
	
	private String getContentFromUrl(String url) {
		String sourceLine;
	    String content = "";
	        
	    // The URL address of the page to open.
	    URL address = null;
	    URLConnection urlConnection = null;
	       
		try {
				address = new URL(url);
				urlConnection = address.openConnection();
		} catch (MalformedURLException e) {
				e.printStackTrace();
				return "FAILED";
		} catch (IOException e) {
				e.printStackTrace();
				return "FAILED";
		}
		urlConnection.setReadTimeout(60000);
		urlConnection.setConnectTimeout(60000);
		urlConnection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
	    InputStream pageInput = null;
		try {
			pageInput = urlConnection.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CAUGHT");
			return "FAILED";
		}
		
	    BufferedReader source;
		source = new BufferedReader(new InputStreamReader(pageInput));

	    // Append each new HTML line into one string. Add a tab character.
	    try {
			while ((sourceLine = source.readLine()) != null)
				        content += sourceLine + "\t";
		} catch (IOException e) {
			return "FAILED";
		}
	    
	    return content;
	}
	
	private String replaceToBlank(String content, String pattern) {
		Pattern style = Pattern.compile(pattern);
		Matcher mstyle = style.matcher(content);
		while (mstyle.find()) content = mstyle.replaceAll("");
		return content;
	}
	
	
	private ArrayList<String> getListFromPattern(String content, String pattern) {
	    ArrayList<String> output = new ArrayList<String>();
	    Pattern div_1 = Pattern.compile(pattern);
	    Matcher mscript_1 = div_1.matcher(content);
	    while (mscript_1.find()) {
	        output.add(mscript_1.group());
	    }
	    return output;
	}
}
