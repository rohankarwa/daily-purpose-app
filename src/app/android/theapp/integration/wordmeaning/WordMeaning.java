package app.android.theapp.integration.wordmeaning;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordMeaning {
	
	private String BASE_URL = "http://dictionary.cambridge.org/dictionary/british/";
	private String GENERIC_SEARCH_BASE_URL = "http://dictionary.cambridge.org/spellcheck/american-english/?q=";
	private String SITE_BASE_URL = "http://dictionary.cambridge.org";
	public WordMeaning() { }
	
	public ArrayList<String> getWordMeaning(String word) {
		ArrayList<String> wordMeaning = new ArrayList<String>();
		
		String directUrl = constructUrl(word.trim());
		String content = getContentFromUrl(directUrl);
		
		if(content.equalsIgnoreCase("FAILED")) {
			String wordUrl = doGenericSearchAndReturnUrl(word);
			System.out.println(wordUrl);
			if(wordUrl.equalsIgnoreCase("FAILED")) {
				wordMeaning.add("No Results");
				return wordMeaning;
			}
			
			content = getContentFromUrl(wordUrl);
			if(content.equalsIgnoreCase("FAILED")) {
				wordMeaning.add("No Results");
				return wordMeaning;
			}
		}
		
		wordMeaning = parseContentAndGetMeaning(content);
		return wordMeaning;
	}
	
	private String doGenericSearchAndReturnUrl(String word) {
		String queryWord = generateQueryWord(word);
		String genericSearchUrl = GENERIC_SEARCH_BASE_URL + queryWord;
		String searchContent = getContentFromUrl(genericSearchUrl);
		if(searchContent.equalsIgnoreCase("FAILED")){
			return "FAILED";
		}
		
		ArrayList<String> resultBlocks = getListFromPattern(searchContent, "<li><a class=\"cdo-link\" href=.*?>");
		if(resultBlocks.size() == 0) return "FAILED";
		
		String firstResult = resultBlocks.get(0);
		ArrayList<String> resultSet = getListFromPattern(firstResult, "\".*?\"");
		if(resultSet.size() <= 1) return "FAILED";
		
		String resultUrl = resultSet.get(1);
		if(resultUrl.length() <= 2) return "FAILED";
		
		String resultRelativeUrl = resultUrl.substring(1, resultUrl.length()-1);
		String fullUrl = SITE_BASE_URL + resultRelativeUrl;
		return fullUrl;
	}

	private ArrayList<String> parseContentAndGetMeaning(String content) {
		String wordFromPage = "";
		//Get Word
		ArrayList<String> wordBlocks = 
				getListFromPattern(content, "<span class=\"BASE.*?</span>");
		if(wordBlocks.size() != 0) {
			wordFromPage = "Word: " + replaceToBlank(wordBlocks.get(0), "<.*?>").trim();
			
		}
		//Get Word Meaning
		ArrayList<String> wordMeaningBlocks = 
				getListFromPattern(content, "<span class=\"def parentof__def__is__sense_b\">.*?</span>");
		ArrayList<String> wordMeanings = new ArrayList<String>();
		for(String wordMeaningBlock : wordMeaningBlocks) {
			wordMeaningBlock = replaceToBlank(wordMeaningBlock, "<.*?>");
			wordMeanings.add(wordFromPage + ", Meaning: " + wordMeaningBlock.trim());
		}
		return wordMeanings;
	}

	private String constructUrl(String word) {
		String queryWord = generateQueryWord(word);
		String urlWord = generateUrlWord(word);
		String url = BASE_URL + urlWord + "?q=" + queryWord;
//		String.format(BASE_URL, urlWord, queryWord );
		return url;
	}
	
	
	private String generateUrlWord(String word) {
		StringBuffer newWord = new StringBuffer();
		for(int i =0 ; i < word.length(); i++) {
			if(word.charAt(i) == ' ') {
				newWord.append('-');
			}
			else {
				newWord.append(word.charAt(i));
			}
		}
		return newWord.toString();
	}

	private String generateQueryWord(String word) {
		StringBuffer newWord = new StringBuffer();
		for(int i =0 ; i < word.length(); i++) {
			if(word.charAt(i) == ' ') {
				newWord.append('+');
			}
			else {
				newWord.append(word.charAt(i));
			}
		}
		return newWord.toString();
	}

	private String getContentFromUrl(String url) {
		String sourceLine;
	    String content = "";
	        
	    // The URL address of the page to open.
	    URL address = null;
	    URLConnection urlConnection = null;
	       
		try {
				address = new URL(url);
				urlConnection = address.openConnection();
		} catch (MalformedURLException e) {
				
				return "FAILED";
		} catch (IOException e) {
				
				return "FAILED";
		}
		urlConnection.setReadTimeout(20000);
		urlConnection.setConnectTimeout(20000);
		urlConnection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
	    InputStream pageInput = null;
		try {
			pageInput = urlConnection.getInputStream();
		} catch (Exception e) {
			return "FAILED";
		}
		
	    BufferedReader source;
		source = new BufferedReader(new InputStreamReader(pageInput));

	    // Append each new HTML line into one string. Add a tab character.
	    try {
			while ((sourceLine = source.readLine()) != null)
				        content += sourceLine + "\t";
		} catch (IOException e) {
			return "FAILED";
		}
	    
	    return content;
	}
	
	private String replaceToBlank(String content, String pattern) {
		Pattern style = Pattern.compile(pattern);
		Matcher mstyle = style.matcher(content);
		while (mstyle.find()) content = mstyle.replaceAll("");
		return content;
	}
	
	
	private ArrayList<String> getListFromPattern(String content, String pattern) {
	    ArrayList<String> output = new ArrayList<String>();
	    Pattern div_1 = Pattern.compile(pattern);
	    Matcher mscript_1 = div_1.matcher(content);
	    while (mscript_1.find()) {
	        output.add(mscript_1.group());
	    }
	    return output;
	}
}
