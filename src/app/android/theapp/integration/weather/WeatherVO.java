package app.android.theapp.integration.weather;

import java.util.ArrayList;

public class WeatherVO {
	private String date;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPrecipMM() {
		return precipMM;
	}
	public void setPrecipMM(String precipMM) {
		this.precipMM = precipMM;
	}
	public String getTempMaxC() {
		return tempMaxC;
	}
	public void setTempMaxC(String tempMaxC) {
		this.tempMaxC = tempMaxC;
	}
	public String getTempMinC() {
		return tempMinC;
	}
	public void setTempMinC(String tempMinC) {
		this.tempMinC = tempMinC;
	}
	
	public String getWindspeedKmph() {
		return windspeedKmph;
	}
	public void setWindspeedKmph(String windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}
	public String getWindspeedMiles() {
		return windspeedMiles;
	}
	public void setWindspeedMiles(String windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}
	private String precipMM;
	private String tempMaxC;
	private String tempMinC;
	private ArrayList<WeatherDescVO> weatherDesc;
	public ArrayList<WeatherDescVO> getWeatherDesc() {
		return weatherDesc;
	}
	public void setWeatherDesc(ArrayList<WeatherDescVO> weatherDesc) {
		this.weatherDesc = weatherDesc;
	}
	private String windspeedKmph;
	private String windspeedMiles;
}
