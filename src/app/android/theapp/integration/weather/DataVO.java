package app.android.theapp.integration.weather;

import java.util.ArrayList;

public class DataVO {
	private ArrayList<CurrentConditionVO> current_condition;
	public ArrayList<CurrentConditionVO> getCurrent_condition() {
		return current_condition;
	}
	public void setCurrent_condition(ArrayList<CurrentConditionVO> current_condition) {
		this.current_condition = current_condition;
	}
	public ArrayList<WeatherVO> getWeather() {
		return weather;
	}
	public void setWeather(ArrayList<WeatherVO> weather) {
		this.weather = weather;
	}
	private ArrayList<WeatherVO> weather; 
}
