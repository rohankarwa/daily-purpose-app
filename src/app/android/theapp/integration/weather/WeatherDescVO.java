package app.android.theapp.integration.weather;

public class WeatherDescVO {
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
