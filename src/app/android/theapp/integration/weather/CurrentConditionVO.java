package app.android.theapp.integration.weather;

import java.util.ArrayList;

public class CurrentConditionVO {
	private String humidity;
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getPrecipMM() {
		return precipMM;
	}
	public void setPrecipMM(String precipMM) {
		this.precipMM = precipMM;
	}
	public String getPressure() {
		return pressure;
	}
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}
	public String getTemp_C() {
		return temp_C;
	}
	public void setTemp_C(String temp_C) {
		this.temp_C = temp_C;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	
	public String getWindspeedKmph() {
		return windspeedKmph;
	}
	public void setWindspeedKmph(String windspeedKmph) {
		this.windspeedKmph = windspeedKmph;
	}
	public String getWindspeedMiles() {
		return windspeedMiles;
	}
	public void setWindspeedMiles(String windspeedMiles) {
		this.windspeedMiles = windspeedMiles;
	}
	private String precipMM;
	private String pressure;
	private String temp_C;
	private String visibility;
	private ArrayList<WeatherDescVO> weatherDesc;
	public ArrayList<WeatherDescVO> getWeatherDesc() {
		return weatherDesc;
	}
	public void setWeatherDesc(ArrayList<WeatherDescVO> weatherDesc) {
		this.weatherDesc = weatherDesc;
	}
	private String windspeedKmph;
	private String windspeedMiles;
}
