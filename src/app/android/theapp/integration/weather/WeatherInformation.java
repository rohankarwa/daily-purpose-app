package app.android.theapp.integration.weather;

import java.io.Reader;

import app.android.theapp.integration.common.HTTPCall;

import com.google.gson.Gson;

public class WeatherInformation {
	
	public WeatherInformation() {}
	
	public MainWeather getWeatherInformation(double latitude, double longitude) {
		return getWeatherInformation(Double.toString(latitude), 
								     Double.toString(longitude));
	}
	
	public MainWeather getWeatherInformation(String latitude, String longitude) {
		String url = "http://free.worldweatheronline.com/feed/weather.ashx?q=" + latitude +
					 "," + longitude +
					 "&format=json&num_of_days=5&key=82e4e1e6dd161853120203";
		HTTPCall weatherUrl = new HTTPCall();
		Reader content = weatherUrl.getContentFromUrl(url);
		Gson gson = new Gson();
		MainWeather weatherInformation = gson.fromJson(content, MainWeather.class);
/*		
		System.out.println("Temp_C = " + weatherInformation.getData().getCurrent_condition().get(0).getTemp_C());
		System.out.println("Day2 = " + weatherInformation.getData().getWeather().get(1).getDate());
		System.out.println("TempMaxC = " + weatherInformation.getData().getWeather().get(1).getTempMaxC());
*/		
		return weatherInformation;
	}
}
