package app.android.theapp.integration.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HTTPCall {

	public Reader getContentFromUrl(String url) {
		//String sourceLine;
	    //String content = "";
	        
	    // The URL address of the page to open.
	    URL address = null;
	    URLConnection urlConnection = null;
	       
		try {
				address = new URL(url);
				urlConnection = address.openConnection();
		} catch (MalformedURLException e) {
				e.printStackTrace();
		//		return "FAILED";
		} catch (IOException e) {
				e.printStackTrace();
		//		return "FAILED";
		}
		urlConnection.setReadTimeout(60000);
		urlConnection.setConnectTimeout(60000);
		urlConnection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
	    InputStream pageInput = null;
		try {
			pageInput = urlConnection.getInputStream();
		} catch (Exception e) {
			e.printStackTrace();
		//	return "FAILED";
		}
		
	    Reader source;
		source = new BufferedReader(new InputStreamReader(pageInput));
		
		return source;
	}
	    

}
