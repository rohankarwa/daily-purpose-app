package app.android.theapp.integration.bmi;

public class BMI {

	private String height;
	private String weight;
	
	public BMI(String height, String weight) {
		this.height = removeSpaces(height.trim()); 
		this.weight = removeSpaces(weight.trim());
	}
	
	private String removeSpaces(String originalString) {
		char dot = '.';
		StringBuffer newString = new StringBuffer();
		for(int i=0; i < originalString.length(); i++) {
		//	System.out.println(originalString.charAt(i));
			if(isItNumber(originalString.charAt(i))) {
				newString.append(originalString.charAt(i));
			}
			else {
				newString.append(dot);
			}
				
		}
		return newString.toString();
	}

	private boolean isItNumber(char charCheck) {
		switch (charCheck){
			case '1' : return true;
			case '2' : return true;
			case '3' : return true;
			case '4' : return true;
			case '5' : return true;
			case '6' : return true;
			case '7' : return true;
			case '8' : return true;
			case '9' : return true;
			case '0' : return true;
		}
		return false;
	}

	public String getBMI() {
		try {
			
			float heightFloat = Float.parseFloat(height);
			float heightInMeter = (float)(heightFloat/100);
			float weightFloat = Float.parseFloat(weight);
			float bmi = (float)(weightFloat/(heightInMeter * heightInMeter));
			return Float.toString(bmi);
		} catch (Exception e) {
			return "Bad Values entered in the Text";
		}
	}
}
