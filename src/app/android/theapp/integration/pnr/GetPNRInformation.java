package app.android.theapp.integration.pnr;

import java.io.Reader;

import app.android.theapp.integration.common.HTTPCall;

import com.google.gson.Gson;

public class GetPNRInformation {
	
	public PNRStatus getPNRStatus(String pnr) {
		HTTPCall pnrUrl = new HTTPCall();
		Reader content = pnrUrl.getContentFromUrl("http://pnrapi.appspot.com/" + pnr);
		Gson gson = new Gson();
		PNRStatus status = gson.fromJson(content, PNRStatus.class);
		
		System.out.println("Data = " + status.getData());
		System.out.println("Status = " + status.getStatus());
		
		return status;
	}
}
