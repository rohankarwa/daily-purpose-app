package app.android.theapp.integration.pnr;

import java.util.ArrayList;

public class PNRInfo {
	private String train_name;
	private String train_number;
	private String from;
	private String to;
	private String alight;
	private String board;
	private String travel_date;
	private ArrayList<Passenger> passenger;
	
	public String getTrain_name() {
		return train_name;
	}
	public void setTrain_name(String train_name) {
		this.train_name = train_name;
	}
	public String getTrain_number() {
		return train_number;
	}
	public void setTrain_number(String train_number) {
		this.train_number = train_number;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getAlight() {
		return alight;
	}
	public void setAlight(String alight) {
		this.alight = alight;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getTravel_date() {
		return travel_date;
	}
	public void setTravel_date(String travel_date) {
		this.travel_date = travel_date;
	}
	public ArrayList<Passenger> getPassenger() {
		return passenger;
	}
	public void setPassenger(ArrayList<Passenger> passenger) {
		this.passenger = passenger;
	}

}
