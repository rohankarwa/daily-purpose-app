package app.android.theapp;



import java.util.ArrayList;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import app.android.theapp.integration.locationfinder.MyLocation;
import app.android.theapp.integration.locationfinder.MyLocation.LocationResult;
import app.android.theapp.integration.weather.MainWeather;
import app.android.theapp.integration.weather.WeatherInformation;
import app.android.theapp.integration.weather.WeatherVO;

public class WeatherActivity extends Activity{
	
	private Button weatherRefreshButton;
	private ListView weatherResultListView;
	private TextView weatherLocationTextview;
	
	MyLocation myLocation = new MyLocation();
	
	private void locationClick() {
		
	    myLocation.getLocation(this, locationResult);
	}

	public LocationResult locationResult = new LocationResult(){
	    @Override
	    public void gotLocation(final Location location){
	    	
	    	WeatherInformation weatherInformation = new WeatherInformation();
	    	MainWeather mainWeather = weatherInformation.getWeatherInformation(location.getLatitude(), 
	    											location.getLongitude());
	    	weatherLocationTextview.setText("Latitude: " + location.getLatitude() + ", Longitude: " + location.getLongitude());
	    	setDisplayResultList(mainWeather);
	    }
	};

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather);
        weatherRefreshButton = (Button) findViewById(R.id.weatherRefresh);
        weatherLocationTextview = (TextView) findViewById(R.id.weatherLocation);
        weatherResultListView = (ListView) findViewById(R.id.weatherResultList);
        locationClick();
        
        weatherRefreshButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				locationClick();}
		});
	}

	private void setDisplayResultList(MainWeather mainWeather) {
		
		ArrayList<String> resultFormat = convertWeatherInformationToResult(mainWeather);
		
		ListAdapter listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, resultFormat );
		weatherResultListView.setAdapter(listAdapter);
		
	}

	private ArrayList<String> convertWeatherInformationToResult(MainWeather mainWeather) {
		ArrayList<String> resultToDisplay = new ArrayList<String>();
		if(mainWeather.getData().getCurrent_condition().size() > 0) {
			String heading       = "Current Condition\n";
			String temperature   = "Current Temperature (C) = " + mainWeather.getData().getCurrent_condition().get(0).getTemp_C() + "\n";
			String humidity      = "Humidity = " 			+ mainWeather.getData().getCurrent_condition().get(0).getHumidity() + "\n";
			String precipitation = "Precipitation (MM) = "  + mainWeather.getData().getCurrent_condition().get(0).getPrecipMM() + "\n";
			String visibility    = "Visibility = " 			+ mainWeather.getData().getCurrent_condition().get(0).getVisibility() + "\n";
			String windSpeed 	 = "Wind Speed (KM/Hr) = "  + mainWeather.getData().getCurrent_condition().get(0).getWindspeedKmph() + "\n";
			String description   = "\n";
			if(mainWeather.getData().getCurrent_condition().get(0).getWeatherDesc().size() > 0)
				description = "Description = " + mainWeather.getData().getCurrent_condition().get(0).getWeatherDesc().get(0).getValue() + "\n";
			String currentCondition = heading + description + temperature + humidity + precipitation + visibility + windSpeed;
			resultToDisplay.add(currentCondition);
		}
		
		for(WeatherVO weatherVO : mainWeather.getData().getWeather()) {
			String heading          = "Forecast Date = " 		       + weatherVO.getDate() + "\n";
			String minTemperature   = "Minimum Temperature (C) = " 	   + weatherVO.getTempMinC() + "\n";
			String maxTemperature   = "Maximum Temperature (C) = "     + weatherVO.getTempMaxC()+ "\n";
			String precipitation    = "Precipitation (MM) = " 		   + weatherVO.getPrecipMM() + "\n";
			String windSpeed 	    = "Wind Speed (KM/Hr) = "  		   + weatherVO.getWindspeedKmph() + "\n";
			String description   = "\n";
			if(weatherVO.getWeatherDesc().size() > 0)
				description = "Description = " + weatherVO.getWeatherDesc().get(0).getValue() + "\n";
			String forecastCondition = heading + description + minTemperature + maxTemperature + precipitation + windSpeed;
			resultToDisplay.add(forecastCondition);
		}
		
		return resultToDisplay;
	}
}
