package app.android.theapp;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import app.android.theapp.integration.horoscope.Horoscope;

public class HoroscopeActivity extends Activity{
	private ArrayList<String> sunSigns;
	private Spinner spinner;
	private Button horoscopeButton;
	private TextView resultTextView;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horoscope);
        sunSigns = prepareSunsignList();
        
        spinner = (Spinner) findViewById(R.id.horoscopeSpinner);
        horoscopeButton = (Button) findViewById(R.id.horoscopeButton);
        resultTextView = (TextView) findViewById(R.id.horoscopeResult);
        
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sunSigns);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
			//	int index = arg0.getSelectedItemPosition();
			//	Toast.makeText(getBaseContext(), "You have selected item: " + sunSigns.get(index), 
			//						Toast.LENGTH_SHORT).show();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		
        });
        
        horoscopeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				resultTextView.setText("");
				Horoscope horoscope = new Horoscope();
				String selectedSunSign = spinner.getSelectedItem().toString();
				String prediction = horoscope.getHoroscope(selectedSunSign);
				resultTextView.setText("Result: " + prediction);
			}
		});
        
	}

	private ArrayList<String> prepareSunsignList() {
		ArrayList<String> sunSigns = new ArrayList<String>();
		sunSigns.add(Horoscope.ARIES);
		sunSigns.add(Horoscope.TAURUS);
		sunSigns.add(Horoscope.GEMINI);
		sunSigns.add(Horoscope.CANCER);
		sunSigns.add(Horoscope.LEO);
		sunSigns.add(Horoscope.VIRGO);
		sunSigns.add(Horoscope.LIBRA);
		sunSigns.add(Horoscope.SCORPIO);
		sunSigns.add(Horoscope.SAGITTARIS);
		sunSigns.add(Horoscope.CAPRICON);
		sunSigns.add(Horoscope.AQUARIUS);
		sunSigns.add(Horoscope.PISCES);
		
		return sunSigns;
	}
	
}
