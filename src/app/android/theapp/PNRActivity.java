package app.android.theapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import app.android.theapp.integration.pnr.GetPNRInformation;
import app.android.theapp.integration.pnr.PNRStatus;

public class PNRActivity extends Activity{
	private Button pnrButton;
	private EditText pnrValueText;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pnr);
        
        pnrButton = (Button) findViewById(R.id.btnPnr);
        pnrValueText = (EditText) findViewById(R.id.pnrEditText);
        
        pnrButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				GetPNRInformation pnrInfo = new GetPNRInformation();
				PNRStatus status = pnrInfo.getPNRStatus(pnrValueText.getText().toString());
				Toast.makeText(getBaseContext(), status.getData() + status.getStatus(), Toast.LENGTH_SHORT).show();
			}
		});
	}
}
