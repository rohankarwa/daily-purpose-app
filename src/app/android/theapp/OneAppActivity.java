package app.android.theapp;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import app.android.theapp.registeration.AppInformation;
import app.android.theapp.registeration.RegisterAppsImpl;
import app.android.theapp.registeration.RegisterAppsInterface;

public class OneAppActivity extends Activity {
	
	private ListView listView;
	private HashMap<String, String> allAppInfoHashMap;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        RegisterAppsInterface registerApps = new RegisterAppsImpl();
        ArrayList<AppInformation> allAppInformation = registerApps.registerAllApps();
        
        ArrayList<String> allAppNames = getAllAppName(allAppInformation);
        setListView(allAppNames);
        
        allAppInfoHashMap = createAllAppInfoHashMap(allAppInformation);
        registerListClickListener(allAppInformation);
    }	

	private HashMap<String, String> createAllAppInfoHashMap(ArrayList<AppInformation> allAppInformation) {
		HashMap<String, String> allAppInfoHashMap = new HashMap<String, String>();
		for(AppInformation currentAppInformation : allAppInformation) {
			allAppInfoHashMap.put(currentAppInformation.getAppName(), currentAppInformation.getAppIntent());
		}
		return allAppInfoHashMap;
	}

	private void registerListClickListener(ArrayList<AppInformation> allAppInformation) {
		
		listView.setOnItemClickListener(new OnItemClickListener() {
		    
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		          
		  //        Toast.makeText(getApplicationContext(), allAppInfoHashMap.get(((TextView) view).getText()),
		  //            Toast.LENGTH_SHORT).show();
		          startActivity(new Intent(allAppInfoHashMap.get(((TextView) view).getText())));
		    }
		});
	}

	private ArrayList<String> getAllAppName(ArrayList<AppInformation> allAppInformation) {
		ArrayList<String> allAppNames = new ArrayList<String>();
		
		for(AppInformation currentAppInformation : allAppInformation) {
			allAppNames.add(currentAppInformation.getAppName());
		}
		return allAppNames;
	}

	private void setListView(ArrayList<String> allAppNames) {
		listView = (ListView) findViewById(R.id.listView);
        
        ListAdapter listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, allAppNames );
        listView.setAdapter(listAdapter);
	}
}