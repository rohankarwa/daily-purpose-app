package app.android.theapp;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import app.android.theapp.integration.locationfinder.MyLocation;
import app.android.theapp.integration.locationfinder.MyLocation.LocationResult;

public class CurrentLocationActivity extends Activity{
	private TextView displayPosition;
	
	MyLocation myLocation = new MyLocation();
	
	private void locationClick() {
		
	    myLocation.getLocation(this, locationResult);
	}

	public LocationResult locationResult = new LocationResult(){
	    @Override
	    public void gotLocation(final Location location){
	    	
	    	Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
	    	String addressFromLocator = "";
	    	try {
	    		List<Address> addresses = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
	    		
	    		if(addresses.size() > 0) {
	    			for(int i=0; i < addresses.get(0).getMaxAddressLineIndex(); i++) {
	    				addressFromLocator = addressFromLocator + addresses.get(0).getAddressLine(i) + "\n";
	    			}
	    		}
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    	
	        displayPosition.setText("Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude() +
	        		", Address = " + addressFromLocator);
	        
	        Toast.makeText(getBaseContext(), "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude(), Toast.LENGTH_SHORT).show();
	    }
	};

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currentlocation);
        displayPosition = (TextView) findViewById(R.id.CurrentPositionDisplay);
        
        locationClick();
        
        Button calculateLocation = (Button) findViewById(R.id.btnLocation);
        calculateLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				locationClick();
				
			}
		});
        
        Button smsButton = (Button) findViewById(R.id.smsLocation);
        Button emailButton = (Button) findViewById(R.id.emailLocation);
        
        smsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
				smsIntent.putExtra("sms_body", displayPosition.getText().toString());
				smsIntent.setType("vnd.android-dir/mms-sms");
				startActivity(smsIntent);
			}
		});
        
        emailButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String currentTime = dateFormat.format(cal.getTime());
				String subject = "My Latest Know Location at time: " + currentTime;
				String emailBody = displayPosition.getText().toString();
				String[] to = {};
				String[] cc = {};
				
				sendEmail(to, cc, subject, emailBody);

			}
		});
	}
	
	private void sendEmail(String[] to, String[] cc, String subject, String message) {
    	Intent emailIntent = new Intent(Intent.ACTION_SEND);
    	emailIntent.setData(Uri.parse("mailto:"));
    	emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
    	emailIntent.putExtra(Intent.EXTRA_CC, cc);
    	emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
    	emailIntent.putExtra(Intent.EXTRA_TEXT, message);
    	emailIntent.setType("message/rfc822");
    	startActivity(Intent.createChooser(emailIntent, "Email"));
    }
}
