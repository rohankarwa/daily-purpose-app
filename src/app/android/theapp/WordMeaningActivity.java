package app.android.theapp;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import app.android.theapp.integration.wordmeaning.WordMeaning;

public class WordMeaningActivity extends Activity{
	private Button wordMeaningButton;
	private ListView wordMeaningListView;
	private EditText wordMeaningEditText;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wordmeaning);
        
        wordMeaningButton = (Button) findViewById(R.id.wordMeaningbtn);
        wordMeaningEditText = (EditText) findViewById(R.id.wordMeaningEditText);
        wordMeaningListView = (ListView) findViewById(R.id.wordMeaningListView);
        
        wordMeaningButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String wordEntered = wordMeaningEditText.getText().toString();
				WordMeaning wordMeaning = new WordMeaning();
				ArrayList<String> listOfWordMeanings = wordMeaning.getWordMeaning(wordEntered);

				setResultList(listOfWordMeanings);
			}
		});
        
	}
	
	private void setResultList(ArrayList<String> listOfWordMeanings) {
		        
        ListAdapter listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listOfWordMeanings );
        wordMeaningListView.setAdapter(listAdapter);
	}
}
